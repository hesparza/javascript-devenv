import {expect} from 'chai';
import jsdom from 'jsdom';
import fs from 'fs'; //node's file system utility..

describe('Our first test', function() {
  it('should pass', function() {
    expect(true).to.equal*true
  });
});

describe('index.html', function() {
  it('should say Users', function(done) {
    const index = fs.readFileSync('./src/index.html', 'utf-8');
    jsdom.env(index, function(err, window) {
      const h1 =  window.document.getElementsByTagName('h1')[0];
      expect(h1.innerHTML).to.equal('Users');
      done();//Need to tell mocha that the test is done, since it is running in an asynchronous function (callback function)
      window.close();
    });
  });
});
